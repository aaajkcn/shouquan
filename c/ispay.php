<?php
  require 'oauth.php';
// 判断是否支付
    class ispay extends Control{
       
       public function _GET(){
            session_start();
       	       //??用session获取授权用户openID
            $id  =$_SESSION['id'];
            // var_dump($id);

            $fgid = $_POST['fgid']; //活动用户id
            // var_dump($fgid);
            //如果活动id不为空，链表查询一下活动id的分享图片
            if($fgid != 'no'){
                $result = $this->sql('huodong.wx_con')->select('*')->join(["inner join wx_ceshi on wx_ceshi.id = gid"])->where(['gid='=>$fgid])->query(); 
                // var_dump($result);exit;

            }
            //如果查询到了活动id的信息，则把分享的照片查找出来
            if ($result) {
            	$data['image'] = $_SESSION['shareimg'];//分享人fgid的图片  
                //获得金额的总和
                $data['money'] = 0;
            	foreach ($result as $key => $value) {
            		if (isset($result[$key]['isgive'])&&($result[$key]['isgive'] == 1)) {
            			$data['money'] += $value['gmoney'];	
            		}
            	}
                $data['data'] = $result;
            	$this->display('ispayshow',$data);//将载入视图展示列表页
             }
        }
                //ajax链接走到这个方法
                public function pay_GET(){
                        // 检测到愿意支付存一下授权登录id和钱数和wx_con里面的openID（后面用来判断是否支付过），进入到函数ismoney中
                        if (isset($_POST['gid2'])) {//授权登录id
                            $data = $_POST['gid2'];
                            $money = $_POST['money'];
                            $openid = $_POST['openid'];//wx_con中的openid，与wx_ceshi中的openid相同，用来记录是谁付的钱
                            $this->ismoney($data,$money,$openid);
                        }
                        //不愿意支付
                        if (isset($_POST['gid1'])) {//授权登录id
                            $data = $_POST['gid1'];
                            $money = $_POST['money'];
                            $openid = $_POST['openid'];
                            $this->nomoney($data,$money,$openid);
                        }
                }
                 public function ismoney($data,$money,$openid){
                    $data = [
                        'gid'=>$data,//授权活动id
                        'gmoney'=>$money,
                        'openid'=>$openid,//wx_con表中的openID
                        'isgive'=>1,
                        'create_time'=>time()
                    ];
                    $isadd = $this->sql('huodong.wx_con')->insert($data);
                    $_SESSION['wx_openid'] = $isadd['openid'];
                    if ($isadd) {
                        //验证是否存在添加上，愿意支付返回状态码为1
                        echo json_encode(array('status'=>1));
                    }
                }
                //不愿意支付
                public function nomoney($data,$money,$openid){
                     $data = [
                        'gid'=>$data,
                        'gmoney'=>$money,
                        'openid'=>$openid,
                        'isgive'=>0,
                        'create_time'=>time()
                    ];
                    $isadd = $this->sql('huodong.wx_con')->insert($data);
                    if ($isadd) {
                        //验证是否存在添加上，不愿意支付返回状态码为0
                        echo json_encode(array('status'=>0));
                    }
                }
    }

        
        // }
  

